# onur bash aliases

# programlar
alias rsync='rsync -av'
alias vi='vim -Nu NONE'
alias bc='bc -l'

# git extensions
alias gl='g lg'
alias gpl='g pull'
alias gs='gst'

# cesitli
alias imzala='gpg -bas'
alias password-generator="tr -cd '[:alnum:]' < /dev/urandom | fold -w32 | head -n1"

# calendar
alias cal='ncal -M -3 -wb'

# nc aliases
#alias kcnc='export KUBECONFIG=$(ncc kubeconfig)'
#alias n='ncc'

# xorg only aliases
if [ -f "$HOME/.xinitrc" ]; then
  alias monitorukapat='xset dpms force off'
  alias xclip='xclip -r -selection clipboard'
fi
