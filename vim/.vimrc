"       ██▒   █▓ ██▓ ███▄ ▄███▓ ██▀███   ▄████▄
"      ▓██░   █▒▓██▒▓██▒▀█▀ ██▒▓██ ▒ ██▒▒██▀ ▀█
"       ▓██  █▒░▒██▒▓██    ▓██░▓██ ░▄█ ▒▒▓█    ▄
"        ▒██ █░░░██░▒██    ▒██ ▒██▀▀█▄  ▒▓▓▄ ▄██▒
"  ██▓    ▒▀█░  ░██░▒██▒   ░██▒░██▓ ▒██▒▒ ▓███▀ ░
"  ▒▓▒    ░ ▐░  ░▓  ░ ▒░   ░  ░░ ▒▓ ░▒▓░░ ░▒ ▒  ░
"  ░▒     ░ ░░   ▒ ░░  ░      ░  ░▒ ░ ▒░  ░  ▒
"  ░        ░░   ▒ ░░      ░     ░░   ░ ░
"   ░        ░   ░         ░      ░     ░ ░
"   ░       ░                           ░
"
"
" Copyright (c) 2015-2024 Onur Aslan <onuraslan@gmail.com>
"
" This work is free. You can redistribute it and/or modify it under the
" terms of the Do What The Fuck You Want To Public License, Version 2,
" as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.


set nocompatible              " disable vi compatibility
set expandtab                 " expand tabs, use spaces instead of tabs
set shiftwidth=4              " shiftwidth spaces to use for each indent
set tabstop=4                 " tabstop spaces <Tab> in the file counts for
set number                    " show line numbers
set numberwidth=4             " with of line numbers
set dir=$HOME/.vim/swp/       " swap directory
set laststatus=2              " always show status line
set undofile                  " enable undofile support
set undodir=$HOME/.vim/undo/  " directory for undo files
set background=dark           " default vim background is dark
set colorcolumn=81            " show a color in column 81
set clipboard=unnamedplus     " share clipboard between vim sessions
set cursorline                " highlight current line number
set titlestring=%f            " titlestring is just file name
set mouse=a                   " set mouse mode to all (previous modes)
syntax on                     " syntax highlighting always on
let mapleader = ";"           " leader key
colorscheme nord              " color scheme
filetype plugin indent on     " allow plugins to indent

" filetype specific options
autocmd FileType html setlocal shiftwidth=2 tabstop=2 colorcolumn=100
autocmd FileType vue setlocal shiftwidth=2 tabstop=2 colorcolumn=100
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2
autocmd FileType vim setlocal shiftwidth=2 tabstop=2
autocmd FileType rust setlocal colorcolumn=100
autocmd FileType vimwiki setlocal nowrap

" new file templates
autocmd BufNewFile *.html 0r $HOME/.vim/templates/skeleton.html
autocmd BufNewFile *.c 0r $HOME/.vim/templates/skeleton.c
autocmd BufNewFile *.pl 0r $HOME/.vim/templates/skeleton.pl
autocmd BufNewFile *.cc 0r $HOME/.vim/templates/skeleton.cc
autocmd BufNewFile *.cpp 0r $HOME/.vim/templates/skeleton.cc
autocmd BufNewFile *.vue 0r $HOME/.vim/templates/skeleton.vue
autocmd BufNewFile docker-compose*.yml 0r $HOME/.vim/templates/docker-compose.yml

" python highlights
autocmd BufRead,BufNewFile *.py let python_highlight_all=1

" key bindings
" jk is escape
inoremap jk <ESC>
" remove trailing whitespaces
nnoremap <Leader>rtw :%s/\s\+$//e<CR>
" quick save
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
" quick git
nnoremap <Leader>g :Git<CR>
" rg under cursor
nnoremap <Leader>rg :Rg <C-R><C-W><CR>

" guioptions
if has("gui_running")
  set guifont=Hack\ 10

  " set lrbLR and -lrbLR to remove scrollbars
  set guioptions+=lrbLR
  set guioptions-=lrbLR

  " remove menubar and toolbar as well
  set guioptions-=m
  set guioptions-=T

  " Make tabline looks like text
  set guioptions-=e
endif


" plugins
" https://github.com/ctrlpvim/ctrlp.vim
" https://github.com/othree/eregex.vim
" https://github.com/junegunn/fzf.git
" https://github.com/junegunn/fzf.vim.git
" https://github.com/itchyny/lightline.vim
" https://github.com/preservim/nerdcommenter
" https://github.com/scrooloose/nerdtree
" https://github.com/arcticicestudio/nord-vim
" https://github.com/tpope/vim-fugitive
" https://github.com/ntpeters/vim-better-whitespace
" https://github.com/girishji/vimcomplete.git
" https://github.com/airblade/vim-gitgutter
" https://github.com/nathanaelkane/vim-indent-guides
" https://github.com/onur/vim-motivate
" https://github.com/hashivim/vim-terraform.git
" https://github.com/vimwiki/vimwiki

" vimcomlete
let g:vimcomplete_tab_enable = 1

" lightline
let g:lightline = {
  \ 'colorscheme': 'nord',
  \ 'active': {
  \   'left': [ [ 'mode', 'paste' ],
  \             [ 'gitbranch' ],
  \             [ 'readonly', 'relativepath', 'modified' ] ],
  \   'right': [ [ 'trailing', 'lineinfo' ],
  \              ['percent'],
  \              [ 'fileformat', 'fileencoding', 'filetype' ] ]
  \ },
  \ 'component': {
  \   'readonly': '%{&filetype=="help"?"":&readonly?"⭤":""}',
  \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}'
  \ },
  \ 'component_visible_condition': {
  \   'readonly': '(&filetype!="help"&& &readonly)',
  \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
  \   'fugitive': '(exists("*FugitiveHead") && ""!=FugitiveHead)'
  \ },
  \ 'component_expand': {
  \   'trailing': 'TrailingSpaceWarning'
  \ },
  \ 'component_type': {
  \   'trailing': 'warning'
  \ },
  \ 'component_function': {
  \   'gitbranch': 'FugitiveHead'
  \ },
  \ 'tabline': {
  \     'left': [ [ 'tabs' ] ],
  \     'right': [ [ 'close' ] ] }
  \ }

function! s:ftMatches(ft_name)
  return &ft =~ a:ft_name
endfunction

function! TrailingSpaceWarning()
  if s:ftMatches('help') || winwidth(0) < 80 | return '' | endif
  let l:trailing = search('\s$', 'nw')
  return (l:trailing != 0) ? '… trailing[' . trailing . ']' : ''
endfunction

" gitgutter
let g:gitgutter_sign_added = '★'
let g:gitgutter_sign_modified = '☉'
let g:gitgutter_sign_removed = '☆'
let g:gitgutter_sign_modified_removed = '☆'
highlight GitGutterAdd guifg=#8c9440
highlight GitGutterChange guifg=#de935f
highlight GitGutterDelete guifg=#cc6666

" nerdtree
map <F2> :NERDTreeClose \| :NERDTree . <enter>

" vimwiki
let g:vimwiki_list = [{'path': '$HOME/code/vimwiki/docs',
  \ 'syntax': 'markdown', 'ext': 'md', 'index': 'index' }]
let g:vimwiki_global_ext = 0

" vim-indent-guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
let g:indent_guides_color_change_percent = 3

" vim-better-whitespace
highlight ExtraWhitespace guibg=#cc6666

" fzf.vim
let g:fzf_colors = {
  \ 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment']
\ }

" aliases
command Baumappe lcd $HOME/atolye15/baumappe
